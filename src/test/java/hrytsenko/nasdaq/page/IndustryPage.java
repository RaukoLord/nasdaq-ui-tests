package hrytsenko.nasdaq.page;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import java.util.List;
import java.util.stream.Collectors;

import com.codeborne.selenide.Selenide;

/**
 * Page with the companies by industry.
 * 
 * @author hrytsenko.anton
 */
public class IndustryPage extends Page<IndustryPage> {

    private static final String PAGE_URL = "http://www.nasdaq.com/screening/companies-by-industry.aspx";

    /**
     * Open page without filtering.
     * 
     * @return the opened page.
     */
    public static IndustryPage open() {
        Selenide.open(PAGE_URL);
        return new IndustryPage();
    }

    /**
     * Open page with filtering by industry.
     * 
     * @param industry
     *            the industry for filtering.
     * 
     * @return the opened page.
     */
    public static IndustryPage open(String industry) {
        Selenide.open(String.join("", PAGE_URL, "?industry=", industry.replace(" ", "+")));
        return new IndustryPage();
    }

    private IndustryPage() {
    }

    /**
     * Get the page title.
     * 
     * @return the title.
     */
    public String title() {
        return $("#main-content-div h1").text();
    }

    /**
     * Get the list of industries.
     * 
     * @return the list of names of industries.
     */
    public List<String> industries() {
        return $$("#industryshowall a").stream().map(e -> e.text()).collect(Collectors.toList());
    }

    /**
     * Get the active filters.
     * 
     * @return the names of filters.
     */
    public List<String> activeFilters() {
        return $$("#industryshowall .current").stream().map(e -> e.text()).collect(Collectors.toList());
    }

    /**
     * Show the ticker.
     * 
     * <p>
     * Expects that ticker is hidden.
     * 
     * @return this page for chaining.
     */
    public IndustryPage showTicker() {
        $("#indexToggle").shouldHave(text("Show Ticker")).click();
        return this;
    }

    /**
     * Hide the ticker.
     * 
     * <p>
     * Expects that ticker is shown.
     * 
     * @return this page for chaining.
     */
    public IndustryPage hideTicker() {
        $("#indexToggle").shouldHave(text("Hide Ticker")).click();
        return this;
    }

    /**
     * Get the list of major indices from the ticker.
     * 
     * <p>
     * Expects that ticker is shown.
     * 
     * @return the list of names of major indices.
     */
    public List<String> majorIndices() {
        return $("#pfooter").shouldBe(visible).$$("#indexContainer li:nth-child(n+2) a").stream().map(e -> e.text())
                .collect(Collectors.toList());
    }

}
