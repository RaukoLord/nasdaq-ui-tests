package hrytsenko.nasdaq.page;

import java.util.function.Consumer;

/**
 * Page of application.
 * 
 * @author hrytsenko.anton
 */
public abstract class Page<T extends Page<T>> {

    /**
     * Performs the action on page.
     *
     * Expected that the action ensures that assertion is correct.
     * 
     * @param consumer
     *            action to perform on page.
     * 
     * @return this page for chaining.
     */
    public final T ensure(Consumer<T> consumer) {
        consumer.accept(self());
        return self();
    }

    @SuppressWarnings("unchecked")
    private T self() {
        return (T) this;
    }

}
